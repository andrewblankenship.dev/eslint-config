module.exports = {
      "parser": "@angular-eslint/template-parser",
      "plugins":[
        "@angular-eslint/template",
        "@angular-eslint/eslint-plugin-template"
      ],
      "extends": [
        "plugin:@angular-eslint/template/recommended"
      ],
      "rules": {
        "@angular-eslint/template/banana-in-box": "error",
        "@angular-eslint/template/eqeqeq": "error",
        "@angular-eslint/template/no-negated-async": "error"
      }
    }